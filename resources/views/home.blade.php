@extends('layouts.master')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-2"></div>
        <div class="col-md-8">
            <div class="card">
                <h1 class="text-center">Dashboard</h1>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <h3 class="text-center">{{ __('Welcome to our application') }}</h3>

                  <p class="text-center">  You are logged in!</p>
                </div>
            </div>
        </div>
        <div class="col-md-2">
      </div>
    </div>
</div>
@endsection
