@extends('layouts.master')

@push('styles')
@endpush

@push('scripts')
<script>
  jQuery(document).ready(function($){
    $("#opensis_form").validate({
      submitHandler: function(form) {
        send_ajax_request();
    }
    });
    /* $('#sdate').datepicker();
    $('#edate').datepicker(); */
  });





  $( function() {
    var dateFormat = "mm/dd/yy",
      from = $( "#sdate" )
        .datepicker({
          defaultDate: "+1w",
          changeMonth: true,
          numberOfMonths: 2
        })
        .on( "change", function() {
          to.datepicker( "option", "minDate", getDate( this ) );
        }),
      to = $( "#edate" ).datepicker({
        defaultDate: "+1w",
        changeMonth: true,
        numberOfMonths: 2
      })
      .on( "change", function() {
        from.datepicker( "option", "maxDate", getDate( this ) );
      });

    function getDate( element ) {
      var date;
      try {
        date = $.datepicker.parseDate( dateFormat, element.value );
      } catch( error ) {
        date = null;
      }
      return date;
    }
  } );

  function send_ajax_request() {
    $("#msg").removeClass();
    @if($school??NULL)
      $.ajax({
         type:'PATCH',
         url:baseUrl+'/api/schools/{{$school->id}}',
         data:$("#opensis_form").serialize(),
         success:function(data) {
            $("#msg").html("updated");
            $("#msg").addClass('alert alert-success');
         },
         error: function (reject) {
           var $err_msg = reject.responseJSON.message;
           $err_msg += '<ul>';
           var errors = reject.responseJSON.errors;
            $.each(errors, function (key, val) {
              $err_msg += "<li>"+val[0]+"</li>";
            });
            $err_msg += '<ul>';
            $("#msg").append($err_msg);
           $("#msg").addClass('alert alert-danger');
        }
      });
    @else
      $.ajax({
        type:'POST',
        url:baseUrl+'/api/schools',
        data:$("#opensis_form").serialize(),
        success:function(data) {
          $("#msg").html("School added successfully");
          $("#msg").addClass('alert alert-success');
          $("#opensis_form")[0].reset();
        },
        error: function (reject) {
          var $err_msg = reject.responseJSON.message;
          $err_msg += '<ul>';
          var errors = reject.responseJSON.errors;
           $.each(errors, function (key, val) {
             $err_msg += "<li>"+val[0]+"</li>";
           });
           $err_msg += '<ul>';
           $("#msg").append($err_msg);
          $("#msg").addClass('alert alert-danger');
       }
      });
    @endif
  }

</script>
@endpush

@section('content')
<h1>{{ __('Add Information') }}</h1>
@include('layouts.notifications')
@if ($errors->any())
<div class="alert alert-error">

    <ul>
        @foreach ($errors->all() as $message)
            <li>{{ $message }}</li>
        @endforeach
    </ul>

</div>
@endif
<div id="msg"></div>
<form role="form" id="opensis_form" method="post" action="@if($school??NULL){{route('school.update',['id'=>$school->id])}}@else{{route('school.store')}}@endif">
    {!! csrf_field(); !!}
    @if($school??NULL)
      {{method_field('PATCH')}}
    @else
      {{method_field('POST')}}
    @endif
        <div class="form-group col-xs-12 col-sm-6 col-md-6 col-lg-6">
            <label for="SchoolName">{{ __('School Name') }} <span class="required-fill">*</span></label>
            <input type="text" required name="name" value="{{$school->name??old('name')??NULL}}" class="form-control" id="schoolname">
        </div>
        <div class="form-group col-xs-12 col-sm-6 col-md-6 col-lg-6">
            <label for="Address">{{ __('Address')}}</label>
            <input type="text" name="address" value="{{$school->address??old('address')??NULL}}" class="form-control" id="address">
        </div>
        <div class="clearfix"></div>
        <div class="form-group col-xs-12 col-sm-6 col-md-6 col-lg-6">
            <label for="City">{{ __('City') }}</label>
            <input type="text" name="city" value="{{$school->city??old('city')??NULL}}" class="form-control" id="city">
        </div>
        <div class="form-group col-xs-12 col-sm-6 col-md-6 col-lg-6">
            <label for="State">{{ __('State') }}</label>
            <input type="text" name="state" value="{{$school->state??old('state')??NULL}}" class="form-control" id="state">
        </div>
		<div class="form-group col-xs-12 col-sm-6 col-md-6 col-lg-6">
            <label for="Code">{{ __('Zip/Postal Code') }}</label>
            <input type="number" name="zip_code" value="{{$school->zip_code??old('zip_code')??NULL}}" class="form-control" id="zipcode">
        </div>
		<div class="form-group col-xs-12 col-sm-6 col-md-6 col-lg-6">
      <label for="Code">{{ __('Area Code') }}</label>
            <input type="text" name="area_code" value="{{$school->area_code??old('area_code')??NULL}}" class="form-control" id="telephone">
        </div>
		<div class="form-group col-xs-12 col-sm-6 col-md-6 col-lg-6">
      <label for="Telephone">{{ __('Telephone') }}</label>
            <input type="number"  name="telephone" value="{{$school->telephone??old('telephone')??NULL}}" class="form-control" id="principal">
        </div>
		<div class="form-group col-xs-12 col-sm-6 col-md-6 col-lg-6">
      <label for="Principal">{{ __('Principal') }}</label>
            <input type="text" name="principal" value="{{$school->principal??old('principal')??NULL}}" class="form-control" id="base">
        </div>
		<div class="form-group col-xs-12 col-sm-6 col-md-6 col-lg-6">
            <label for="Base">{{ __('Base Grading Scale') }} <span class="required-fill">*</span></label>
            <input type="text" required name="grading_scale" value="{{$school->grading_scale??old('grading_scale')??NULL}}" class="form-control" id="base">
        </div>
		<div class="form-group col-xs-12 col-sm-6 col-md-6 col-lg-6">
            <label for="email"> {{ __('E-Mail') }}</label>
            <input type="email" name="email" value="{{$school->email??old('email')??NULL}}" class="form-control" id="email">
        </div>
		<div class="form-group col-xs-12 col-sm-6 col-md-6 col-lg-6">
            <label for="Website">{{ __('Website') }}</label>
            <input type="website" name="website" value="{{$school->website??old('website')??NULL}}" class="form-control" id="website">
        </div>
		<div class="form-group col-xs-12 col-sm-6 col-md-6 col-lg-6">
            <label for="sdate">{{ __('Start Date') }} <span class="required-fill">*</span></label>
            <input type="text" required name="start_date" value="{{$school->start_date??old('start_date')??NULL}}" class="form-control datepicker_trigger" id="sdate">
        </div>
		<div class="form-group col-xs-12 col-sm-6 col-md-6 col-lg-6">
            <label for="edate">{{ __('End Date') }} <span class="required-fill">*</span></label>
            <input type="text" required name="end_date" value="{{$school->end_date??old('end_date')??NULL}}" class="form-control datepicker_trigger" id="edate">
        </div>
        <div class="clearfix"></div>
        <br>
        <div class="col-xs-10 col-sm-4 col-md-4 col-lg-4">
            <input type="submit" class="btn btn-danger" value="{{ __('Submit') }}" />
        </div>
    </form>
@endsection
