@extends('layouts.master')

@push('styles')
@endpush

@push('scripts')
<script>
jQuery(document).ready(function($) {
  $.ajax({
     type:'GET',
     url:baseUrl+'/api/schools/',
     success:function(data) {

       if(data.length > 0) {
           $.each(data, function (key,school) {

            $('#school_table tbody').append(form_row(school));
          });
        } else {
          $('#school_table tbody').append(form_empty_row());

        }

       // $('#school_table').
     },
     error: function (reject) {
       var $err_msg = reject.responseJSON.message;
       $err_msg += '<ul>';
       var errors = reject.responseJSON.errors;
        $.each(errors, function (key, val) {
          $err_msg += "<li>"+val[0]+"</li>";
        });
        $err_msg += '<ul>';
        $("#msg").append($err_msg);
       $("#msg").addClass('alert alert-danger');
    }
  });
});

function form_row($data) {
  $html = '<tr id="school_row_'+$data.id+'">';
  $html += "<td>"+$data.name+"</td>";
  $html += "<td>"+$data.grading_scale+"</td>";
  $html += "<td>"+$data.start_date+"</td>";
  $html += "<td>"+$data.end_date+"</td>";
  $html += '<td><a href="'+baseUrl+'/schools/edit/'+$data.id+'"><i class="fa fa-edit"></i> Edit</a></td>';
  $html += '<td><a href="javascript:;" onclick="delete_school('+$data.id+')"><i class="fa fa-trash"></i> Delete</a><form style="display:none;" method="post" id="delete_school_'+$data.id+'" action="'+baseUrl+'/school/'+$data.id+'"><input type="hidden" name="_method" value="DELETE"></form></td>';
  $html += '</tr>';

  return $html;
}
function form_empty_row() {
  $html = '<tr>';
  $html += "<td colspan='6'>No results found...</td>";
  $html += '</tr>';

  return $html;
}

function delete_school($id){
  if (confirm('Are you sure to delete this school?')) {

    $.ajax({
       type:'DELETE',
       url:baseUrl+'/api/schools/'+$id,
       data:$("#delete_school_"+$id).serialize(),
       success:function(data) {
          $("#msg").html("School Deleted successfully");
          $("#school_row_"+$id).remove();
          $("#msg").addClass('alert alert-success');
       },
       error: function (reject) {
         var $err_msg = reject.responseJSON.message;
         $err_msg += '<ul>';
         var errors = reject.responseJSON.errors;
          $.each(errors, function (key, val) {
            $err_msg += "<li>"+val[0]+"</li>";
          });
          $err_msg += '<ul>';
          $("#msg").append($err_msg);
         $("#msg").addClass('alert alert-danger');
      }
    });
    return false;
  } else { return false; }
}

</script>
@endpush

@section('content')
<h1>{{ __('School Information') }}</h1>
@include('layouts.notifications')
<div id="msg"></div>
<div class="table-responsive">
<table id="school_table" class="table table-responsiive">
  <thead>
    <tr>
      <th scope="col">{{ __('School Name') }}</th>
      <th scope="col">{{ __('Grade') }}</th>
      <th scope="col">{{ __('Start Date') }}</th>
      <th scope="col">{{ __('End Date') }}</th>
      {{--<th scope="col">{{ __('View') }}</th>--}}
      <th scope="col">{{ __('Edit') }}</th>
      <th scope="col">{{ __('Delete') }}</th>
    </tr>
  </thead>
  <tbody>
    {{--
      @foreach($schools as $school)
      <tr>
        <td>{{$school->name??NULL}}</td>
        <td>{{$school->grading_scale??NULL}}</td>
        <td>@if($school->start_date??NULL){{date('Y M d',strtotime($school->start_date))}}@endif</td>
        <td>@if($school->end_date??NULL){{date('Y M d',strtotime($school->end_date))}}@endif</td>
        {{--<td><a href="{{route('school.edit',['id' => $school->id])}}"><i class="fa fa-eye"></i> {{ __('View') }}</a></td>
  	    <td><a href="{{route('school.edit',['id' => $school->id])}}"><i class="fa fa-edit"></i> {{ __('Edit') }}</a</td>
  	    <td><a href="{{route('school.delete',['id' => $school->id])}}" onclick="if (confirm('Are you sure to delete this school?')) { document.getElementById('delete_school_{{$school->id}}').submit(); return false; } else { return false; }"><i class="fa fa-trash"></i> {{ __('Delete') }}</a>
        <form method="post" id="delete_school_{{$school->id}}" action="{{route('school.delete',['id'=>$school->id])}}">
          {!! csrf_field(); !!}
          {{method_field('DELETE')}}
        </form></td>
      </tr>
    @endforeach
    --}}
  </tbody>
</table>
</div>

@endsection
