<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
  <title>{{ config('app.name', 'Laravel') }}</title>
  <meta charset="utf-8">
  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ asset('css/custom.css') }}">
  <link rel="stylesheet" href="{{ asset('css/font-awesome.css') }}">

  @stack('styles')
  <!-- Styles -->
  {{--<link href="{{ asset('css/app.css') }}" rel="stylesheet">--}}
  <!-- Scripts -->
  {{--<script src="{{ asset('js/app.js') }}" defer></script>--}}
  <script>
    var baseUrl = "{{ url('/') }}";
    console.log(baseUrl);
  </script>
</head>
<body>
  <section class="top_header">
    <div class="container">
      <div class="row">
        <div class="col-lg-6">
          <a title="{{ config('app.name', 'Laravel') }}" href="{{ url('/') }}">
            <img src="{{ asset('images/opensis.png') }}" class="img-responsive">
          </a>
          </div>
          <div class="col-lg-6">
            <div id="google_translate_element"></div>
          </div>
        </div>
      </div>
    </section>
    <section class="menu-header">
      <nav class="navbar navbar-default">
        <div class="container">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">MENU</a>
          </div>
          <div id="navbar" class="navbar-collapse collapse ">
            <ul class="nav navbar-nav navbar-right">
              @guest
                  <li class="nav-item">
                      <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                  </li>
                  @if (Route::has('register'))
                      <li class="nav-item">
                          <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                      </li>
                  @endif
              @else
                <li class="nav-item dropdown">
                    <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                        {{ Auth::user()->name }} <span class="caret"></span>
                    </a>

                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="{{ route('logout') }}"
                           onclick="event.preventDefault();
                                         document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </div>
                </li>
            @endguest
              <li><a href="{{route('school.add')}}"><i class="fa fa-plus" aria-hidden="true"></i> Add School</a></li>
              <li><a href="{{route('school.index')}}"><i class="fa fa-eye" aria-hidden="true"></i> View School</a></li>
            </ul>
          </div><!--/.nav-collapse -->
        </div>
      </nav>
    </section>
    <section class="content-inner">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            @yield('content')
          </div>
        </div>
      </div>
    </section>

    <section class="footer">
      <div class="container">
      </div class="row">
      <div class="col-lg-12">
        <p class="text-center">copyright@2019 | Developed by Creosen</p>
      </div>
    </div>
  </div>
</section>
<script type="text/javascript">
function googleTranslateElementInit() {
  new google.translate.TranslateElement({pageLanguage: 'en'}, 'google_translate_element');
}
</script>

<script type="text/javascript" src="{{ asset('js/translate.js') }}"></script>
<script src="{{ asset('js/jquery.js') }}"></script>
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.1/dist/jquery.validate.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>

</script>
@stack('scripts')
</body>
</html>
