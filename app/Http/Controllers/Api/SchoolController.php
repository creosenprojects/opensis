<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\School as SchoolResource;
use App\Http\Resources\SchoolCollection;
use Illuminate\Support\Facades\Validator;
use App\School;

class SchoolController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      // return new SchoolResource(School::first());
      // return new SchoolCollection(School::paginate(10));
      return response()->json(new SchoolCollection(School::all()),200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        Validator::make($request->all(), [
            'name' => 'required|unique:schools|max:191',
            'grading_scale' => 'required|numeric',
            'email' => 'nullable|email',
            'website' => 'nullable|url',
            'start_date' => 'nullable|date',
            'end_date' => 'nullable|date',
        ])->validate();

        $school = new School();
        $start_date = NULL;
        $end_date = NULL;

        if($request->start_date??NULL) {
          $start_date = date('Y-m-d',strtotime($request->start_date));
        }
        if($request->end_date??NULL) {
          $end_date = date('Y-m-d',strtotime($request->end_date));
        }
        $school->name = $request->name??NULL;
        $school->grading_scale = $request->grading_scale??NULL;
        $school->address = $request->address??NULL;
        $school->city = $request->city??NULL;
        $school->state = $request->state??NULL;
        $school->zip_code = $request->zip_code??NULL;
        $school->area_code = $request->area_code??NULL;
        $school->telephone = $request->telephone??NULL;
        $school->principal = $request->principal??NULL;
        $school->email = $request->email??NULL;
        $school->website = $request->website??NULL;
        $school->start_date = $start_date;
        $school->end_date = $end_date;
        $school->save();

        // return new SchoolResource($school);
        return response()->json(new SchoolResource($school),200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
      // return new SchoolResource(School::FindOrFail($id));

      return response()->json(new SchoolResource(School::FindOrFail($id)),200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $school = School::findOrFail($id);

      Validator::make($request->all(), [
          'name' => 'required|unique:schools,name,'.$id.'|max:191',
          'grading_scale' => 'required|numeric',
          'email' => 'nullable|email',
          'website' => 'nullable|url',
          'start_date' => 'required|date',
          'end_date' => 'required|date',
      ])->validate();

      if($school??NULL) {

        $start_date = NULL;
        $end_date = NULL;

        if($request->start_date??NULL) {
          $start_date = date('Y-m-d',strtotime($request->start_date));
        }
        if($request->end_date??NULL) {
          $end_date = date('Y-m-d',strtotime($request->end_date));
        }
        $school->name = $request->name??NULL;
        $school->grading_scale = $request->grading_scale??NULL;
        $school->address = $request->address??NULL;
        $school->city = $request->city??NULL;
        $school->state = $request->state??NULL;
        $school->zip_code = $request->zip_code??NULL;
        $school->area_code = $request->area_code??NULL;
        $school->telephone = $request->telephone??NULL;
        $school->principal = $request->principal??NULL;
        $school->email = $request->email??NULL;
        $school->website = $request->website??NULL;
        $school->start_date = $start_date;
        $school->end_date = $end_date;
        $school->save();

      }

      // return new SchoolResource($school);

      return response()->json(new SchoolResource($school),200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $school = School::findOrFail($id);
        $school->delete();

        // return new SchoolResource($school);
        return response()->json(new SchoolResource($school),200);
    }
}
