<?php

namespace App\Http\Controllers;

use App\School;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;


class SchoolsController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index() {
      $schools = School::get();
      return view('front/schools/index',compact('schools'));
    }

    public function add() {
      return view('front/schools/edit',compact('schools'));
    }

    public function store(Request $request) {

      Validator::make($request->all(), [
          'name' => 'required|unique:schools|max:191',
          'grading_scale' => 'required|numeric',
          'email' => 'nullable|email',
          'website' => 'nullable|url',
          'start_date' => 'nullable|date',
          'end_date' => 'nullable|date',
      ])->validate();

      $start_date = NULL;
      $end_date = NULL;

      if($request->start_date??NULL) {
        $start_date = date('Y-m-d',strtotime($request->start_date));
      }
      if($request->end_date??NULL) {
        $end_date = date('Y-m-d',strtotime($request->end_date));
      }

      $school = School::create(['name' => $request->name,'grading_scale' => $request->grading_scale]);
      if($school??NULL){
        $school->address = $request->address??NULL;
        $school->city = $request->city??NULL;
        $school->state = $request->state??NULL;
        $school->zip_code = $request->zip_code??NULL;
        $school->area_code = $request->area_code??NULL;
        $school->telephone = $request->telephone??NULL;
        $school->principal = $request->principal??NULL;
        $school->email = $request->email??NULL;
        $school->website = $request->website??NULL;
        $school->start_date = $start_date;
        $school->end_date = $end_date;
        $school->save();

        return redirect(route('school.index'))->with('success',__('School added successfully'));
      }

      return redirect(route('school.index'))->with('error',__('Something went wrong'));

    }

    public function edit($id) {
      $school = School::find($id);
      if($school??NULL){
        if($school->start_date??NULL) {
          $school->start_date = date('m/d/Y',strtotime($school->start_date));
        }
        if($school->end_date??NULL) {
          $school->end_date = date('m/d/Y',strtotime($school->end_date));
        }
        return view('front/schools/edit',compact('school'));
      }
      return redirect(route('school.index'))->with('error',__('Something went wrong'));
    }

    public function update(Request $request,$id) {
      $school = School::find($id);
      if($school??NULL){

        Validator::make($request->all(), [
            'name' => 'required|unique:schools,name,'.$id.'|max:191',
            'grading_scale' => 'required|numeric',
            'email' => 'nullable|email',
            'website' => 'nullable|url',
            'start_date' => 'required|date',
            'end_date' => 'required|date',
        ])->validate();

        $start_date = NULL;
        $end_date = NULL;

        if($request->start_date??NULL) {
          $start_date = date('Y-m-d',strtotime($request->start_date));
        }
        if($request->end_date??NULL) {
          $end_date = date('Y-m-d',strtotime($request->end_date));
        }
        $school->name = $request->name??NULL;
        $school->grading_scale = $request->grading_scale??NULL;
        $school->address = $request->address??NULL;
        $school->city = $request->city??NULL;
        $school->state = $request->state??NULL;
        $school->zip_code = $request->zip_code??NULL;
        $school->area_code = $request->area_code??NULL;
        $school->telephone = $request->telephone??NULL;
        $school->principal = $request->principal??NULL;
        $school->email = $request->email??NULL;
        $school->website = $request->website??NULL;
        $school->start_date = $start_date;
        $school->end_date = $end_date;
        $school->save();

        return redirect(route('school.index'))->with('success',__('School updated successfully'));
      }

      return redirect(route('school.index'))->with('error',__('Something went wrong'));

    }

    public function delete($id) {
      $school = School::find($id);
      if($school??NULL){
        $school->delete();
        return redirect(route('school.index'))->with('success',__('School deleted successfully'));
      }
      return redirect(route('school.index'))->with('error',__('Something went wrong'));
    }

}
