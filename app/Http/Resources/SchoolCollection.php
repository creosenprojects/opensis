<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class SchoolCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
      return parent::toArray($request);
      // return [
      //     'id' => $this->id,
      //     'name' => $this->name,
      //     'address' => $this->address,
      //     'city' => $this->city,
      //     'state' => $this->state,
      //     'zip_code' => $this->zip_code,
      //     'area_code' => $this->area_code,
      //     'telephone' => $this->telephone,
      //     'principal' => $this->principal,
      //     'grading_scale' => $this->grading_scale,
      //     'email' => $this->email,
      //     'website' => $this->website,
      //     'start_date' => $this->start_date,
      //     'end_date' => $this->end_date
      //   ];
    }
}
