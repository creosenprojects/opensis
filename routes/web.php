<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/home');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/schools', 'SchoolsController@index')->name('school.index');
Route::get('/schools/add', 'SchoolsController@add')->name('school.add');
Route::post('/schools/store', 'SchoolsController@store')->name('school.store');
Route::get('/schools/edit/{id}', 'SchoolsController@edit')->name('school.edit');
Route::patch('/schools/update/{id}', 'SchoolsController@update')->name('school.update');
Route::delete('/schools/delete/{id}', 'SchoolsController@delete')->name('school.delete');

Route::get('locale/{locale}', function ($locale){
    Session::put('locale', $locale);
    return redirect()->back();
});
